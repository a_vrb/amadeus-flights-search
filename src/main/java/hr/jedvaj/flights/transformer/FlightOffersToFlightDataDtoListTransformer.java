package hr.jedvaj.flights.transformer;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amadeus.resources.FlightOffer;
import com.amadeus.resources.FlightOffer.OfferItem;
import com.amadeus.resources.FlightOffer.Segment;
import com.amadeus.resources.FlightOffer.Service;

import hr.jedvaj.flights.model.FlightDataDTO;
import hr.jedvaj.flights.model.SearchData;

public class FlightOffersToFlightDataDtoListTransformer {
	private static final Logger logger = LoggerFactory.getLogger(FlightOffersToFlightDataDtoListTransformer.class);

	public List<FlightDataDTO> transform(FlightOffer[] flightOffers, SearchData searchData) {
		logger.debug("Transforming response data to dto object (FlightOffers > List<FlightDataDTO>)");

		List<FlightDataDTO> flightDataList = new ArrayList<>();

		Integer passengerCount = searchData.getAdults() + searchData.getChildren() + searchData.getInfants()
				+ searchData.getSeniors();

		for (FlightOffer flightOffer : flightOffers) {
			for (OfferItem offeritem : flightOffer.getOfferItems()) {
				FlightDataDTO dataDto = new FlightDataDTO();
				dataDto.setPassengerCount(passengerCount);
				dataDto.setCurrency(searchData.getCurrency());

				Double totalPrice = Double.valueOf(offeritem.getPrice().getTotal())	+ Double.valueOf(offeritem.getPrice().getTotalTaxes());
				dataDto.setTotalPrice(totalPrice);
				
				List<Service> services = Arrays.asList(offeritem.getServices());
				Service goingFlight = services.get(0);
				List<Segment> toSegments = Arrays.asList(goingFlight.getSegments());
				OffsetDateTime toFlightTime = OffsetDateTime.parse(toSegments.get(0).getFlightSegment().getDeparture().getAt());

				dataDto.setOrigin(toSegments.get(0).getFlightSegment().getDeparture().getIataCode());
				dataDto.setDepartureDate(toFlightTime.toLocalDateTime());
				dataDto.setTransfersOnGoingFlight(toSegments.size() - 1);

				if (services.size() > 1) {
					Service returningFlight = services.get(1);
					List<Segment> fromSegments = Arrays.asList(returningFlight.getSegments());
					OffsetDateTime fromFlightTime = OffsetDateTime.parse(fromSegments.get(0).getFlightSegment().getDeparture().getAt());

					dataDto.setDestination(fromSegments.get(0).getFlightSegment().getDeparture().getIataCode());
					dataDto.setReturnDate(fromFlightTime.toLocalDateTime());
					dataDto.setTransfersOnReturnFlight(fromSegments.size() - 1);
				}
				flightDataList.add(dataDto);
			}
		}

		return flightDataList;
	}
}
