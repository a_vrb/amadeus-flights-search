package hr.jedvaj.flights.dao.impl;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.amadeus.Amadeus;
import com.amadeus.Params;
import com.amadeus.exceptions.ResponseException;
import com.amadeus.resources.FlightOffer;

import hr.jedvaj.flights.dao.AmadeusDao;
import hr.jedvaj.flights.model.FlightDataDTO;
import hr.jedvaj.flights.model.SearchData;
import hr.jedvaj.flights.transformer.FlightOffersToFlightDataDtoListTransformer;

@Service
@CacheConfig(cacheNames={"flightOffers"})
public class AmadeusDaoImpl implements AmadeusDao {
	
	private static final Logger logger = LoggerFactory.getLogger(AmadeusDaoImpl.class);
	
	private final Amadeus amadeusClient;
	private final FlightOffersToFlightDataDtoListTransformer flightDataTransformer;

	
	@Autowired
	public AmadeusDaoImpl(Amadeus amadeusClient, FlightOffersToFlightDataDtoListTransformer flightDataTransformer) {
		Assert.notNull(amadeusClient, "Amadeus must not be null!");
		Assert.notNull(flightDataTransformer, "FlightOfferListToFlightDataDtoListTransformer must not be null!");

		this.amadeusClient = amadeusClient;
		this.flightDataTransformer = flightDataTransformer;
	}


	@Override
	@Cacheable
	public List<FlightDataDTO> getFlightOffers(SearchData searchData) {
		logger.debug("Calling Amadeus API with input parameters {}", searchData);
		
		try {
			FlightOffer[] flightOffers = amadeusClient.shopping.flightOffers.get(Params.with("origin", searchData.getOrigin())
					.and("destination", searchData.getDestination())
					.and("departureDate", searchData.getDepartureDate().toString())
					.and("returnDate", searchData.getReturnDate().toString())
					.and("adults",searchData.getAdults())
					.and("children",searchData.getChildren())
					.and("infants",searchData.getInfants())
					.and("seniors",searchData.getSeniors())
					.and("currency",searchData.getCurrency().toString())
					.and("max", 250));
			
			
			return flightDataTransformer.transform(flightOffers, searchData);
		} catch (ResponseException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
		
	}
	
}
