package hr.jedvaj.flights.dao;

import java.util.List;

import hr.jedvaj.flights.model.FlightDataDTO;
import hr.jedvaj.flights.model.SearchData;

public interface AmadeusDao {

	List<FlightDataDTO> getFlightOffers(SearchData searchData);

}