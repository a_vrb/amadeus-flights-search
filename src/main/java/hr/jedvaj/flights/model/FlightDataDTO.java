package hr.jedvaj.flights.model;

import java.io.Serializable;
import java.time.LocalDateTime;

public class FlightDataDTO implements Serializable {

	private static final long serialVersionUID = -6157239184983971036L;

	private String origin;
	
	private String destination;
	
	private LocalDateTime departureDate;
	
	private LocalDateTime returnDate;
	
	private Integer passengerCount;
	
	private Integer transfersOnGoingFlight;
	
	private Integer transfersOnReturnFlight;
	
	private Double totalPrice;
	
	private Currency currency;
	
	public FlightDataDTO() {}
	
	public FlightDataDTO(String origin, String destination, LocalDateTime departureDate, LocalDateTime returnDate,
			Integer passengerCount, Integer transfersOnGoingFlight, Integer transfersOnReturnFlight, Double totalPrice, Currency currency) {
		super();
		this.origin = origin;
		this.destination = destination;
		this.departureDate = departureDate;
		this.returnDate = returnDate;
		this.passengerCount = passengerCount;
		this.transfersOnGoingFlight = transfersOnGoingFlight;
		this.transfersOnReturnFlight = transfersOnReturnFlight;
		this.totalPrice = totalPrice;
		this.currency = currency;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public LocalDateTime getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(LocalDateTime departureDate) {
		this.departureDate = departureDate;
	}

	public LocalDateTime getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(LocalDateTime returnDate) {
		this.returnDate = returnDate;
	}

	public Integer getPassengerCount() {
		return passengerCount;
	}

	public void setPassengerCount(Integer passengerCount) {
		this.passengerCount = passengerCount;
	}

	public Integer getTransfersOnGoingFlight() {
		return transfersOnGoingFlight;
	}

	public void setTransfersOnGoingFlight(Integer transfersOnGoingFlight) {
		this.transfersOnGoingFlight = transfersOnGoingFlight;
	}

	public Integer getTransfersOnReturnFlight() {
		return transfersOnReturnFlight;
	}

	public void setTransfersOnReturnFlight(Integer transfersOnReturnFlight) {
		this.transfersOnReturnFlight = transfersOnReturnFlight;
	}

	public Double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	@Override
	public String toString() {
		return "FlightDataDTO [origin=" + origin + ", destination=" + destination + ", departureDate=" + departureDate
				+ ", returnDate=" + returnDate + ", passengerCount=" + passengerCount + ", transfersOnGoingFlight="
				+ transfersOnGoingFlight + ", transfersOnReturnFlight=" + transfersOnReturnFlight + ", currency="
				+ currency + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((currency == null) ? 0 : currency.hashCode());
		result = prime * result + ((departureDate == null) ? 0 : departureDate.hashCode());
		result = prime * result + ((destination == null) ? 0 : destination.hashCode());
		result = prime * result + ((origin == null) ? 0 : origin.hashCode());
		result = prime * result + ((passengerCount == null) ? 0 : passengerCount.hashCode());
		result = prime * result + ((returnDate == null) ? 0 : returnDate.hashCode());
		result = prime * result + ((totalPrice == null) ? 0 : totalPrice.hashCode());
		result = prime * result + ((transfersOnGoingFlight == null) ? 0 : transfersOnGoingFlight.hashCode());
		result = prime * result + ((transfersOnReturnFlight == null) ? 0 : transfersOnReturnFlight.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FlightDataDTO other = (FlightDataDTO) obj;
		if (currency != other.currency)
			return false;
		if (departureDate == null) {
			if (other.departureDate != null)
				return false;
		} else if (!departureDate.equals(other.departureDate))
			return false;
		if (destination == null) {
			if (other.destination != null)
				return false;
		} else if (!destination.equals(other.destination))
			return false;
		if (origin == null) {
			if (other.origin != null)
				return false;
		} else if (!origin.equals(other.origin))
			return false;
		if (passengerCount == null) {
			if (other.passengerCount != null)
				return false;
		} else if (!passengerCount.equals(other.passengerCount))
			return false;
		if (returnDate == null) {
			if (other.returnDate != null)
				return false;
		} else if (!returnDate.equals(other.returnDate))
			return false;
		if (totalPrice == null) {
			if (other.totalPrice != null)
				return false;
		} else if (!totalPrice.equals(other.totalPrice))
			return false;
		if (transfersOnGoingFlight == null) {
			if (other.transfersOnGoingFlight != null)
				return false;
		} else if (!transfersOnGoingFlight.equals(other.transfersOnGoingFlight))
			return false;
		if (transfersOnReturnFlight == null) {
			if (other.transfersOnReturnFlight != null)
				return false;
		} else if (!transfersOnReturnFlight.equals(other.transfersOnReturnFlight))
			return false;
		return true;
	}
	
	
}
