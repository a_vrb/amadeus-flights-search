package hr.jedvaj.flights.model;

public enum Currency {
	USD, EUR, HRK;
}
