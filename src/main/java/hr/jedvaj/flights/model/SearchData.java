package hr.jedvaj.flights.model;

import java.io.Serializable;
import java.time.LocalDate;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.springframework.format.annotation.DateTimeFormat;

public class SearchData implements Serializable{

	private static final long serialVersionUID = -3067892525868105227L;

	@NotNull 
	@Pattern(regexp = "[A-Z]{3}")
	private String origin;
	
	@NotNull 
	@Pattern(regexp = "[A-Z]{3}")
	private String destination;
	
	@NotNull
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate departureDate;
	
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate returnDate;

	@NotNull 
	@Min(0) @Max(9)
	private Integer adults;

	@NotNull 
	@Min(0) @Max(9)
	private Integer children;
	
	@NotNull 
	@Min(0) @Max(9)
	private Integer infants;

	@NotNull 
	@Min(0) @Max(9)
	private Integer seniors;
	
	private Currency currency;

	public SearchData() {}

	public SearchData(String origin, String destination, LocalDate departureDate, LocalDate returnDate, Integer adults,
			Integer children, Integer infants, Integer seniors, Currency currency) {
		this.origin = origin;
		this.destination = destination;
		this.departureDate = departureDate;
		this.returnDate = returnDate;
		this.adults = adults;
		this.children = children;
		this.infants = infants;
		this.seniors = seniors;
		this.currency = currency;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public LocalDate getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(LocalDate departureDate) {
		this.departureDate = departureDate;
	}

	public LocalDate getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(LocalDate returnDate) {
		this.returnDate = returnDate;
	}

	public Integer getAdults() {
		return adults;
	}

	public void setAdults(Integer adults) {
		this.adults = adults;
	}

	public Integer getChildren() {
		return children;
	}

	public void setChildren(Integer children) {
		this.children = children;
	}

	public Integer getInfants() {
		return infants;
	}

	public void setInfants(Integer infants) {
		this.infants = infants;
	}

	public Integer getSeniors() {
		return seniors;
	}

	public void setSeniors(Integer seniors) {
		this.seniors = seniors;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	@Override
	public String toString() {
		return "SearchData [origin=" + origin + ", destination=" + destination + ", departureDate=" + departureDate
				+ ", returnDate=" + returnDate + ", adults=" + adults + ", children=" + children + ", infants="
				+ infants + ", seniors=" + seniors + ", currency=" + currency + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((adults == null) ? 0 : adults.hashCode());
		result = prime * result + ((children == null) ? 0 : children.hashCode());
		result = prime * result + ((currency == null) ? 0 : currency.hashCode());
		result = prime * result + ((departureDate == null) ? 0 : departureDate.hashCode());
		result = prime * result + ((destination == null) ? 0 : destination.hashCode());
		result = prime * result + ((infants == null) ? 0 : infants.hashCode());
		result = prime * result + ((origin == null) ? 0 : origin.hashCode());
		result = prime * result + ((returnDate == null) ? 0 : returnDate.hashCode());
		result = prime * result + ((seniors == null) ? 0 : seniors.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SearchData other = (SearchData) obj;
		if (adults == null) {
			if (other.adults != null)
				return false;
		} else if (!adults.equals(other.adults))
			return false;
		if (children == null) {
			if (other.children != null)
				return false;
		} else if (!children.equals(other.children))
			return false;
		if (currency != other.currency)
			return false;
		if (departureDate == null) {
			if (other.departureDate != null)
				return false;
		} else if (!departureDate.equals(other.departureDate))
			return false;
		if (destination == null) {
			if (other.destination != null)
				return false;
		} else if (!destination.equals(other.destination))
			return false;
		if (infants == null) {
			if (other.infants != null)
				return false;
		} else if (!infants.equals(other.infants))
			return false;
		if (origin == null) {
			if (other.origin != null)
				return false;
		} else if (!origin.equals(other.origin))
			return false;
		if (returnDate == null) {
			if (other.returnDate != null)
				return false;
		} else if (!returnDate.equals(other.returnDate))
			return false;
		if (seniors == null) {
			if (other.seniors != null)
				return false;
		} else if (!seniors.equals(other.seniors))
			return false;
		return true;
	}

	
	
	
}
