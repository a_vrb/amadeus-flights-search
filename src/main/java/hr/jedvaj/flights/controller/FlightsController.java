package hr.jedvaj.flights.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hr.jedvaj.flights.model.FlightDataDTO;
import hr.jedvaj.flights.model.SearchData;
import hr.jedvaj.flights.service.FlightDataService;

@RestController
@RequestMapping("/api/flights")
public class FlightsController {
	private static final Logger logger = LoggerFactory.getLogger(FlightsController.class);

	private final FlightDataService flightDataService;
	
	@Autowired
	public FlightsController(FlightDataService flightDataService) {
		Assert.notNull(flightDataService, "FlightDataService must not be null");
		
		this.flightDataService = flightDataService;
	}

	@GetMapping
	public ResponseEntity<Page<FlightDataDTO>> getFlightOffers(@Valid SearchData searchData, Pageable pageable){
		logger.debug("Request received for api call \"/api/flights\" wtih data {} and pageable {}", searchData, pageable);
		
		Page<FlightDataDTO> flightData = flightDataService.findFlightData(searchData, pageable);

		return ResponseEntity.ok(flightData);
	}
	
}
