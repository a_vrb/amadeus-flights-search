package hr.jedvaj.flights.controller;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.amadeus.exceptions.ClientException;

import hr.jedvaj.flights.model.FlightDataDTO;
import hr.jedvaj.flights.model.SearchData;
import hr.jedvaj.flights.service.FlightDataService;

@Controller
public class WebController {
	private static final Logger logger = LoggerFactory.getLogger(WebController.class);
	
	private final FlightDataService flightDataService;
	
	@Autowired
	public WebController(FlightDataService flightDataService) {
		Assert.notNull(flightDataService, "FlightDataService must not be null");
		
		this.flightDataService = flightDataService;
	}

	@GetMapping("/")
	public String getIndex(Model model) {
		model.addAttribute("searchData", new SearchData());
		return "index";
	}
	
	@PostMapping("/")
	public String postIndex(@ModelAttribute @Valid SearchData searchData, Pageable pageable, BindingResult bindingResult, Model model) {
		
		if(bindingResult.hasErrors()) {
			return "index";
		}

		Page<FlightDataDTO> flightData = flightDataService.findFlightData(searchData,pageable);
		model.addAttribute("flightDataList", flightData.getContent());
		model.addAttribute("page", flightData);
		
		
		int totalPages = flightData.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                .boxed()
                .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
            model.addAttribute("totalPages", flightData.getTotalPages());
            model.addAttribute("size", flightData.getSize());
        }		
		
		logger.debug("Returning data in controller: {}", flightData);
		return "index";
	}
	
    @ExceptionHandler({ ClientException.class })
    public String handleException(Exception e, Model model) {
		model.addAttribute("searchData", new SearchData());
    	model.addAttribute("amadeusClientError", e.getCause().getMessage());
    	return "index";
    }
	
}
