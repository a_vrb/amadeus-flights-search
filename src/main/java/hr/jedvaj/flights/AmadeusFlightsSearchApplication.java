package hr.jedvaj.flights;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;

import hr.jedvaj.flights.config.AmadeusApiProperties;

@SpringBootApplication
@EnableConfigurationProperties(AmadeusApiProperties.class)
@EnableCaching
public class AmadeusFlightsSearchApplication {

	public static void main(String[] args) {
		SpringApplication.run(AmadeusFlightsSearchApplication.class, args);
	}

}
