package hr.jedvaj.flights.service.impl;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import hr.jedvaj.flights.dao.AmadeusDao;
import hr.jedvaj.flights.model.FlightDataDTO;
import hr.jedvaj.flights.model.SearchData;
import hr.jedvaj.flights.service.FlightDataService;

@Service
public class AmadeusFlightDataService implements FlightDataService {

	private static final Logger logger = LoggerFactory.getLogger(AmadeusFlightDataService.class);

	private final AmadeusDao amadeusDao;

	@Autowired
	public AmadeusFlightDataService(AmadeusDao amadeusDao) {
		Assert.notNull(amadeusDao, "AmadeusDao must not be null!");

		this.amadeusDao = amadeusDao;
	}

	@Override
	public List<FlightDataDTO> findFlightData(SearchData searchData) {
		logger.debug("Loading flight data for parameters {} ", searchData);

		List<FlightDataDTO> flights = amadeusDao.getFlightOffers(searchData);
		flights.sort(Comparator.comparing(flight -> flight.getDepartureDate()));
		
		return flights;
	}
	
	@Override
	public Page<FlightDataDTO> findFlightData(SearchData searchData, Pageable pageable) {
		logger.debug("Load page {} of flight data", pageable.getPageNumber());
		
		List<FlightDataDTO> flights = findFlightData(searchData);
	
		List<FlightDataDTO> filteredFlights = new ArrayList<>();
		for(long i = pageable.getOffset(); i < (pageable.getOffset() + pageable.getPageSize()); i++) {
			if(i >= flights.size()) {
				break;
			}
			filteredFlights.add(flights.get((int)i));
		}
		
		Page<FlightDataDTO> page = new PageImpl<FlightDataDTO>(filteredFlights, pageable, flights.size());
		return page;
	}


}
