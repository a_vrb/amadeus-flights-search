package hr.jedvaj.flights.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import hr.jedvaj.flights.model.FlightDataDTO;
import hr.jedvaj.flights.model.SearchData;

public interface FlightDataService {

	public List<FlightDataDTO> findFlightData(SearchData searchData);
	
	public Page<FlightDataDTO> findFlightData(SearchData searchData, Pageable pageable);
	
}
