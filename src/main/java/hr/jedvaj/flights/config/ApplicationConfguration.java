package hr.jedvaj.flights.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.amadeus.Amadeus;

import hr.jedvaj.flights.transformer.FlightOffersToFlightDataDtoListTransformer;

@Configuration
public class ApplicationConfguration {
	
	@Autowired
	private AmadeusApiProperties amadeusApiProperties;
	
	@Bean
	public Amadeus amadeusClient() {
		Amadeus amadeusClient = Amadeus.builder(amadeusApiProperties.getClientId(), amadeusApiProperties.getClientSecret())
				.setHostname(amadeusApiProperties.getHostname())
				.build();
		return amadeusClient;
	}
	
	@Bean
	public FlightOffersToFlightDataDtoListTransformer flightDataTransformer() {
		return new FlightOffersToFlightDataDtoListTransformer();
	}
}
