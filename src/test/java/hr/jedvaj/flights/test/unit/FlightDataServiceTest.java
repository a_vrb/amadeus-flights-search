package hr.jedvaj.flights.test.unit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.ResourceUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import hr.jedvaj.flights.dao.AmadeusDao;
import hr.jedvaj.flights.model.FlightDataDTO;
import hr.jedvaj.flights.model.SearchData;
import hr.jedvaj.flights.service.FlightDataService;
import hr.jedvaj.flights.service.impl.AmadeusFlightDataService;

@RunWith(SpringRunner.class)
public class FlightDataServiceTest {

	@Mock
	private AmadeusDao amadueusDao;
	
	private ObjectMapper objectMapper;
	
	private FlightDataService flightDataService;
	
	private SearchData searchDataRef;
	private List<FlightDataDTO> flightsRef;
	
	
	@Before
	public void setup() throws JsonParseException, JsonMappingException, IOException {
		this.objectMapper = new ObjectMapper();
		this.objectMapper.registerModule(new JavaTimeModule());
		
		this.searchDataRef = objectMapper.readValue(ResourceUtils.getFile("classpath:json/searchData.json"), SearchData.class);
		this.flightsRef = objectMapper.readValue(ResourceUtils.getFile("classpath:json/flightDataDTO.json"),  new TypeReference<List<FlightDataDTO>>(){});
		
		when(amadueusDao.getFlightOffers(this.searchDataRef)).thenReturn(this.flightsRef);
		
		this.flightDataService = new AmadeusFlightDataService(amadueusDao);
	}
	
	@Test
	public void testFindFlightData_OK() {
		List<FlightDataDTO> findFlightData = this.flightDataService.findFlightData(this.searchDataRef);
		
		assertEquals(flightsRef, findFlightData);
	}
	
	@Test
	public void testFindFlightDataPageable_OK() {
		Page<FlightDataDTO> findFlightData = this.flightDataService.findFlightData(this.searchDataRef, PageRequest.of(3, 5));
		
		assertNotNull(findFlightData);
		assertNotNull(findFlightData.getContent());
		assertEquals(findFlightData.getNumberOfElements(),findFlightData.getContent().size());
		assertEquals(4,findFlightData.getTotalPages());
		assertEquals(20,findFlightData.getTotalElements());
		
		
		
	}
	
}
