package hr.jedvaj.flights.test.integration;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.StreamingHttpOutputMessage.Body;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.util.ResourceUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import hr.jedvaj.flights.dao.AmadeusDao;
import hr.jedvaj.flights.model.FlightDataDTO;
import hr.jedvaj.flights.model.SearchData;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class FlightsControllerTest {
	
    @Autowired
    private MockMvc mockMvc;

	@Mock
	private AmadeusDao amadueusDao;
	
	private SearchData searchDataRef;
	
	@Before
	public void setup() throws JsonParseException, JsonMappingException, FileNotFoundException, IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.registerModule(new JavaTimeModule());
		
		this.searchDataRef = objectMapper.readValue(ResourceUtils.getFile("classpath:json/searchData.json"), SearchData.class);
		List<FlightDataDTO> flightsRef = objectMapper.readValue(ResourceUtils.getFile("classpath:json/flightDataDTO.json"),  new TypeReference<List<FlightDataDTO>>(){});
		
		when(amadueusDao.getFlightOffers(searchDataRef)).thenReturn(flightsRef);
	}
 
	@Test
    public void whenTestFlightController_unauthorized() throws Exception {
        this.mockMvc.perform(get("/api/flights"))
            .andExpect(status().isUnauthorized());
    }
	
    @Test
    @WithMockUser
    public void whenTestFlightController_noParamsBadRequest() throws Exception {
        this.mockMvc.perform(get("/api/flights"))
            .andExpect(status().isBadRequest());
    }
    

    @Test
    @WithMockUser
    public void whenTestFlightController_getFlightDataOK() throws Exception {
    	MockHttpServletRequestBuilder requestBuilder = get("/api/flights")
    			.param("origin", searchDataRef.getOrigin())
    			.param("destination", searchDataRef.getDestination())
    			.param("departureDate", searchDataRef.getDepartureDate().toString())
    			.param("returnDate", searchDataRef.getReturnDate().toString())
    			.param("adults", searchDataRef.getAdults().toString())
    			.param("children", searchDataRef.getChildren().toString())
    			.param("infants", searchDataRef.getInfants().toString())
    			.param("seniors", searchDataRef.getSeniors().toString())
    			.param("currency", searchDataRef.getCurrency().toString())
    			.param("page", "2")
    			.param("size", "5");
    	
    	this.mockMvc.perform(requestBuilder)
            .andExpect(status().isOk());
    }
    
}
