package hr.jedvaj.flights.test.integration;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.amadeus.Amadeus;
import com.amadeus.Params;
import com.amadeus.exceptions.ClientException;
import com.amadeus.exceptions.ResponseException;
import com.amadeus.resources.FlightOffer;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AmadeusApiIntegrationTest {
	
	@Autowired
	private Amadeus amadeusClient;

	
	@Test
	public void testAmadeusClientResponse_OK() throws ResponseException {
		FlightOffer[] flightOffers = amadeusClient.shopping.flightOffers.get(Params.with("origin", "ZAG")
				.and("destination", "CPH")
				.and("departureDate", LocalDate.now().toString())
				.and("max", 5));

		assertNotNull(flightOffers);
		assertTrue(flightOffers.length == 5);
		
	}
	
	@Test(expected = ClientException.class)
	public void testAmadeusClientResponse_Exception() throws ResponseException {
		amadeusClient.shopping.flightOffers.get(Params.with("origin", "ZZZ")
				.and("destination", "CPH")
				.and("departureDate", LocalDate.now().toString())
				.and("max", 5));
	}

}
