# Amadeus Flights Search
Assignment project for KING ICT

## Development Stack

* Spring framework
    * Spring Boot
    * Spring Security
    * Spring MVC 
    * Spring Data
* Thymeleaf
* Amadeus Java SDK
* Hazelcast

## Usage

* Place application.properties file inside <projectRoot>/config/ directory.
* Populate application.properties with minimal required data:
 ```
 #Amadeus API connection properties
hr.jedvaj.flights.amadeus.hostname=test
hr.jedvaj.flights.amadeus.clientId=
hr.jedvaj.flights.amadeus.clientSecret=

#Default user/pass for spring security autoconfiguration
spring.security.user.name=
spring.security.user.password=
  ```

* Additional options that can be provided inside application.properties file:
 ```
#Default Logging levels
logging.level.hr.jedvaj.flights=debug

#Enable/Disable Hazelcast as cache provider
hr.jedvaj.flights.config.hazelcast.enabled=false
  ```

